package com.reactivespring.movieinfoservice.controller;

import com.reactivespring.movieinfoservice.domain.MovieInfo;
import com.reactivespring.movieinfoservice.service.MovieInfoService;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;

@WebFluxTest(controllers = MoviesInfoController.class)
@AutoConfigureWebTestClient
public class MovieInfoControllerUnitTest {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private MovieInfoService movieInfoServiceMock;

    @Test
    void getMovieInfo() {

        var movieinfos = List.of(
                new MovieInfo(null,"Vikram",2021,"film directed by Mani Ratnam",List.of("kamal","Vijay"),LocalDate.parse("2022-09-03")),
                new MovieInfo("123","PS-1",2022,"film directed by Mani Ratnam",List.of("vikaram","karthi"), LocalDate.parse("2022-09-03"))
        );
        when(movieInfoServiceMock.getMovieInfo()).thenReturn(Flux.fromIterable(movieinfos));

        webTestClient
                .get()
                .uri("/v1/movieinfos")
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBodyList(MovieInfo.class)
                .hasSize(2);
    }

    @Test
    void getMovieInfoById() {

        var movieinfos = new MovieInfo("123","PS-1",2022,"film directed by Mani Ratnam",List.of("vikaram","karthi"), LocalDate.parse("2022-09-03"));
        var movieInfoId = "123";

        when(movieInfoServiceMock.getMovieInfoById(isA(String.class))).thenReturn(Mono.just(movieinfos));

        webTestClient
                .get()
                .uri("/v1/movieinfos/{id}", movieInfoId)
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody()
                .jsonPath("$.name").isEqualTo("PS-1");

    }

    @Test
    void addMovieInfo() {

        var movieinfo = new MovieInfo(null,"PS-1",2022, "film directed by Mani Ratnam",
                List.of("vikaram","karthi"), LocalDate.parse("2022-09-03"));

        when(movieInfoServiceMock.addMovieInfo(isA(MovieInfo.class))).thenReturn(Mono.just(
                new MovieInfo("mockid","PS-1",2022, "film directed by Mani Ratnam",
                        List.of("vikaram","karthi"), LocalDate.parse("2022-09-03"))
        ));
        webTestClient
                .post()
                .uri("/v1/movieinfos")
                .bodyValue(movieinfo)
                .exchange()
                .expectStatus()
                .isCreated()
                .expectBody(MovieInfo.class)
                .consumeWith(movieInfoEntityExchangeResult -> {
                    var saveMovieInfo = movieInfoEntityExchangeResult.getResponseBody();
                    assert saveMovieInfo != null;
                    assert saveMovieInfo.getMovieInfoId() != null;
                    assertEquals("mockid",saveMovieInfo.getMovieInfoId());
                });
    }

    @Test
    void addMovieInfo_validation() {

        var movieinfo = new MovieInfo(null,null,-2022,"Ponniyin Selvan: I or PS-1 is an upcoming Indian Tamil-language epic period action film directed by Mani Ratnam, who co-wrote it with Elango Kumaravel and B. Jeyamohan",
                List.of("vikaram","karthi"), LocalDate.parse("2022-09-03"));

        when(movieInfoServiceMock.addMovieInfo(isA(MovieInfo.class))).thenReturn(Mono.just(
                new MovieInfo("mockid","PS-1",2022,"Ponniyin Selvan: I or PS-1 is an upcoming Indian Tamil-language epic period action film directed by Mani Ratnam, who co-wrote it with Elango Kumaravel and B. Jeyamohan",
                        List.of("vikaram","karthi"), LocalDate.parse("2022-09-03"))
        ));
        webTestClient
                .post()
                .uri("/v1/movieinfos")
                .bodyValue(movieinfo)
                .exchange()
                .expectStatus()
                .isBadRequest()
                .expectBody(String.class)
                .consumeWith(movieInfoEntityExchangeResult -> {
                    var responseBody = movieInfoEntityExchangeResult.getResponseBody();
                    assert responseBody != null;
                    System.out.println("ResponseBody : "+ responseBody);
                });
    }

    @Test
    void updateMovieInfo() {

        var movieId = "123";
        var movieinfo = new MovieInfo(null,"PS-2",2022,"film directed by Mani Ratnam",List.of("vikaram","karthi"), LocalDate.parse("2022-09-03"));

        when(movieInfoServiceMock.updateMovieInfo(isA(MovieInfo.class),isA(String.class))).thenReturn(Mono.just(
                new MovieInfo(movieId,"PS-2",2022,"film directed by Mani Ratnam",List.of("vikaram","karthi"), LocalDate.parse("2022-09-03"))
        ));
        webTestClient
                .put()
                .uri("/v1/movieinfos/{id}", movieId)
                .bodyValue(movieinfo)
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody(MovieInfo.class)
                .consumeWith(movieInfoEntityExchangeResult -> {
                    var updatedMovieInfo = movieInfoEntityExchangeResult.getResponseBody();
                    assert updatedMovieInfo != null;
                    assert updatedMovieInfo.getMovieInfoId() != null;
                    assertEquals("PS-2",updatedMovieInfo.getName());
                });
    }

    @Test
    void deleteMovieInfo() {

        var movieId = "123";

        when(movieInfoServiceMock.deleteMovieInfo(isA(String.class))).thenReturn(Mono.empty());
        webTestClient
                .delete()
                .uri("/v1/movieinfos/{id}",movieId)
                .exchange()
                .expectStatus()
                .isNoContent();
    }

}
