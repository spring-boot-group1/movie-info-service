package com.reactivespring.movieinfoservice.controller;

import com.reactivespring.movieinfoservice.domain.MovieInfo;
import com.reactivespring.movieinfoservice.repository.MovieInfoRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ImportAutoConfiguration(exclude = EmbeddedMongoAutoConfiguration.class)
@ActiveProfiles("test")
@AutoConfigureWebTestClient
class MoviesInfoControllerIntgTest {

    @Autowired
    WebTestClient webTestClient;

    @Autowired
    MovieInfoRepository movieInfoRepository;

    @BeforeEach
    void setUp() {

        var movieinfos = List.of(
                new MovieInfo(null,"Vikram",2021,"film directed by Mani Ratnam",List.of("kamal","Vijay"), LocalDate.parse("2022-09-03")),
                new MovieInfo("123","PS-1",2022,"film directed by Mani Ratnam",List.of("vikaram","karthi"), LocalDate.parse("2022-09-03"))
        );
        movieInfoRepository.saveAll(movieinfos).blockLast();
    }

    @AfterEach
    void tearDown() {
        movieInfoRepository.deleteAll().block();
    }

    @Test
    void addMovieInfo() {

        var movieinfo = new MovieInfo(null,"PS-1",2022,"film directed by Mani Ratnam",List.of("vikaram","karthi"), LocalDate.parse("2022-09-03"));

        webTestClient
                .post()
                .uri("/v1/movieinfos")
                .bodyValue(movieinfo)
                .exchange()
                .expectStatus()
                .isCreated()
                .expectBody(MovieInfo.class)
                .consumeWith(movieInfoEntityExchangeResult -> {
                    var saveMovieInfo = movieInfoEntityExchangeResult.getResponseBody();
                    assert saveMovieInfo != null;
                    assert saveMovieInfo.getMovieInfoId() != null;
                });
    }

    @Test
    void getMovieInfo() {

        webTestClient
                .get()
                .uri("/v1/movieinfos")
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBodyList(MovieInfo.class)
                .hasSize(2);
    }

    @Test
    void getMovieInfoByYear() {

        var uri = UriComponentsBuilder.fromUriString("/v1/movieinfos")
                .queryParam("year",2022)
                .buildAndExpand().toUri();

        webTestClient
                .get()
                .uri(uri)
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBodyList(MovieInfo.class)
                .hasSize(1);
    }

    @Test
    void getMovieInfoByName() {

        var uri = UriComponentsBuilder.fromUriString("/v1/movieinfosbyname")
                .queryParam("name","Vikram")
                .buildAndExpand().toUri();

        webTestClient
                .get()
                .uri(uri)
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBodyList(MovieInfo.class)
                .hasSize(1);
    }

    @Test
    void getMovieInfoById() {

        var movieInfoId = "123";
        webTestClient
                .get()
                .uri("/v1/movieinfos/{id}", movieInfoId)
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody()
                .jsonPath("$.name").isEqualTo("PS-1");
//                .expectBody(MovieInfo.class)
//                .consumeWith(movieInfoEntityExchangeResult -> {
//                    var movieInfo = movieInfoEntityExchangeResult.getResponseBody();
//                    assertNotNull(movieInfo);
//                });
    }

    @Test
    void getMovieInfoById_notfound() {

        var movieInfoId = "abc";
        webTestClient
                .get()
                .uri("/v1/movieinfos/{id}", movieInfoId)
                .exchange()
                .expectStatus()
                .isNotFound();
//                .is2xxSuccessful()
//                .expectBody()
//                .jsonPath("$.name").isEqualTo("PS-1");
    }

    @Test
    void updateMovieInfo() {

        var movieId = "123";
        var movieinfo = new MovieInfo(null,"PS-2",2022,"film directed by Mani Ratnam",List.of("vikaram","karthi"), LocalDate.parse("2022-09-03"));

        webTestClient
                .put()
                .uri("/v1/movieinfos/{id}", movieId)
                .bodyValue(movieinfo)
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody(MovieInfo.class)
                .consumeWith(movieInfoEntityExchangeResult -> {
                    var updatedMovieInfo = movieInfoEntityExchangeResult.getResponseBody();
                    assert updatedMovieInfo != null;
                    assert updatedMovieInfo.getMovieInfoId() != null;
                    assertEquals("PS-2",updatedMovieInfo.getName());
                });
    }

    @Test
    void updateMovieInfo_notfound() {

        var movieId = "abc";
        var movieinfo = new MovieInfo(null,"PS-2",2022,"film directed by Mani Ratnam",List.of("vikaram","karthi"), LocalDate.parse("2022-09-03"));

        webTestClient
                .put()
                .uri("/v1/movieinfos/{id}", movieId)
                .bodyValue(movieinfo)
                .exchange()
                .expectStatus()
                .isNotFound();
//                .expectBody(MovieInfo.class)
//                .consumeWith(movieInfoEntityExchangeResult -> {
//                    var updatedMovieInfo = movieInfoEntityExchangeResult.getResponseBody();
//                    assert updatedMovieInfo != null;
//                    assert updatedMovieInfo.getMovieInfoId() != null;
//                    assertEquals("PS-2",updatedMovieInfo.getName());
//                });
    }

    @Test
    void deleteMovieInfo() {

        var movieId = "123";
        webTestClient
                .delete()
                .uri("/v1/movieinfos/{id}",movieId)
                .exchange()
                .expectStatus()
                .isNoContent();

    }
}