package com.reactivespring.movieinfoservice.service;

import com.reactivespring.movieinfoservice.domain.MovieInfo;
import com.reactivespring.movieinfoservice.repository.MovieInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MovieInfoService {
    public Mono<MovieInfo> addMovieInfo(MovieInfo movieInfo);

    public Flux<MovieInfo> getMovieInfo();

    public Mono<MovieInfo> getMovieInfoById(String id) ;

    public Mono<MovieInfo> updateMovieInfo(MovieInfo movieInfo, String id) ;

    public Mono<Void> deleteMovieInfo(String id);

    public Flux<MovieInfo> getMovieInfoByYear(Integer year);

    public Mono<MovieInfo> getMovieInfoByName(String name);
}
